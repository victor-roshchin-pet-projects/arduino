int inputPins[4] = {2, 3, 4, 5};
int ledPins[4] = {10, 11, 12, 13};
int pinSound = 9;

void setup() {
  for (int index = 0; index < 4; index++) {
    pinMode(ledPins[index], OUTPUT);
    pinMode(inputPins[index], INPUT_PULLUP);
  }
  pinMode(pinSound, OUTPUT);
}


void loop() {
  for (int index = 0; index < 4; index++) {
    int val = digitalRead(inputPins[index]);
    if (val == LOW) {
      digitalWrite(ledPins[index], HIGH);
      digitalWrite(pinSound, HIGH);
    } else {
      digitalWrite(ledPins[index], LOW);
      digitalWrite(pinSound, LOW);
    }
  }
}
