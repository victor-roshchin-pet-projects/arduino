#define RED_PIN 8
#define YELLOW_PIN 10
#define GREEN_PIN 12

int redOn = 3000;
int redYellowOn = 1000;
int greenOn = 3000;
int greenBlink = 500;
int yellowOn = 1000;


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(RED_PIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(RED_PIN, HIGH);
  delay(redOn);
  digitalWrite(YELLOW_PIN, HIGH);
  delay(redYellowOn);
  digitalWrite(RED_PIN, LOW);
  digitalWrite(YELLOW_PIN, LOW);
  digitalWrite(GREEN_PIN, HIGH);
  delay(greenOn);
  digitalWrite(GREEN_PIN, LOW);
  for(int i = 0; i < 4; i++) {
    delay(greenBlink);
    digitalWrite(GREEN_PIN, HIGH);
    delay(greenBlink);
    digitalWrite(GREEN_PIN, LOW);
  }
  digitalWrite(YELLOW_PIN, HIGH);
  delay(yellowOn);
  digitalWrite(YELLOW_PIN, LOW);
}
